import { SHOW_EDIT_MODAL, HIDE_EDIT_MODAL } from "../../action-types";

const reducer = (state = false, action) => {
  switch (action.type) {
    case SHOW_EDIT_MODAL:
      return action.payload;
    case HIDE_EDIT_MODAL:
      return false;
    default:
      return state;
  }
}

export default reducer;