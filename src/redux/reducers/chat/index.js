import { combineReducers } from "redux";
import editModalReducer from "./editModal";
import preloaderReducer from "./preloader";
import messagesReducer from "./messages";


const reducers = combineReducers({
  messages: messagesReducer,
  editModal: editModalReducer,
  preloader: preloaderReducer
});

export default reducers;