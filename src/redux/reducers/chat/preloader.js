import { SHOW_PRELOADER, HIDE_PRELOADER } from "../../action-types";

const reducer = (state = true, action) => {
  switch (action.type) {
    case SHOW_PRELOADER:
      return true;
    case HIDE_PRELOADER:
      return false;
    default:
      return state;
  }
}

export default reducer;