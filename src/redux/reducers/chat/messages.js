import { SET_MESSAGES, ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from "../../action-types";

const reducer = (state = [], action) => {
  switch (action.type) {
    case SET_MESSAGES: {
      const messages = action.payload;
      return state = messages;
    }
    case ADD_MESSAGE: {
      const message = action.payload;
      return state = [...state, message];
    }
    case EDIT_MESSAGE: {
      const message = action.payload;

      const newMessages = state.map(oldMessage => {
        if (oldMessage.id === message.id) {
          return message;
        }
        return oldMessage;
      })
      return state = newMessages;
    }
    case DELETE_MESSAGE: {
      const id = action.payload;
      const newMessages = state.filter(message => message.id !== id);
      return state = newMessages;
    }
    default:
      return state;
  }
}

export default reducer;