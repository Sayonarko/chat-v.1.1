import { SHOW_PRELOADER, HIDE_PRELOADER } from "../action-types";


export function showPreloader() {
  return {
    type: SHOW_PRELOADER
  }
}

export function hidePreloader() {
  return {
    type: HIDE_PRELOADER
  }
}