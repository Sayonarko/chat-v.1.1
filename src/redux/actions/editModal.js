import { SHOW_EDIT_MODAL, HIDE_EDIT_MODAL } from "../action-types";

export function showEditModal(message) {
  return {
    type: SHOW_EDIT_MODAL,
    payload: message
  }
}

export function hideEditModal() {
  return {
    type: HIDE_EDIT_MODAL
  }
}