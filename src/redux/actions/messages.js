import { SET_MESSAGES, ADD_MESSAGE, EDIT_MESSAGE, DELETE_MESSAGE } from "../action-types";

export function setMessages(messages) {
  return {
    type: SET_MESSAGES,
    payload: messages
  }
}

export function addMessage(message) {
  return {
    type: ADD_MESSAGE,
    payload: message
  }
}

export function editMessage(message) {
  return {
    type: EDIT_MESSAGE,
    payload: message
  }
}

export function deleteMessage(id) {
  return {
    type: DELETE_MESSAGE,
    payload: id
  }
}