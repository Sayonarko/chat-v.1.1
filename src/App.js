import Chat from "./components/chat";
import Header from "./components/header";

import './App.scss';

const messagesUrl = process.env.REACT_APP_API_URL;

function App() {
  return (
    <div className="App">
      <Header />
      <Chat url={messagesUrl} />
    </div>
  );
}

export default App;
