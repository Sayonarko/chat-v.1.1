import React, { useEffect } from "react";
import Header from "./components/header";
import MessageList from "./components/message-list";
import MessageInput from "./components/messasge-input";
import Preloader from "../preloader";
import MessageEditModal from "./components/message-edit-modal";
import { useDispatch, useSelector } from "react-redux";
import { hidePreloader } from "../../redux/actions/preloader";
import { setMessages, addMessage, deleteMessage } from "../../redux/actions/messages";
import { showEditModal } from "../../redux/actions/editModal";

export default function Chat({ url }) {
  const { messages, username } = useSelector(state => ({
    messages: state.chat.messages,
    username: state.user.username
  }));

  const dispatch = useDispatch();

  const handleAddMessage = message => dispatch(addMessage(message));

  const handleDeleteMessage = id => dispatch(deleteMessage(id));

  const editLastOwnMessage = (e) => {

    if (e.code === "ArrowUp") {
      const ownMessages = messages.filter(message => message.user === username);
      const lastOwnMessage = ownMessages.pop();
      if (lastOwnMessage) dispatch(showEditModal(lastOwnMessage));
    }
  }

  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(data => {
        dispatch(setMessages(data));
        dispatch(hidePreloader());
      })
      .catch(err => console.log(err));
  }, [dispatch, url]);

  return (
    <main className="chat" onKeyUp={editLastOwnMessage} >
      <MessageEditModal />
      <Preloader />
      <Header />
      <MessageList handleDeleteMessage={handleDeleteMessage} />
      <MessageInput handleAddMessage={handleAddMessage} />
    </main>
  )
}