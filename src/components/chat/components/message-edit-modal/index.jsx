import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import { hideEditModal } from "../../../../redux/actions/editModal";
import { editMessage } from "../../../../redux/actions/messages";

import "./style.scss";

export default function MessageEditModal() {
  const message = useSelector(state => state.chat.editModal);
  const isOpen = Boolean(message);
  const [textValue, setTextValue] = useState("");
  const modalRef = useRef();
  const dispatch = useDispatch();

  const onClose = () => dispatch(hideEditModal());

  const onClickClose = e => (e.target === modalRef.current) && onClose();

  const onEditMessage = (e) => {
    e.preventDefault();

    const newMessage = {
      ...message,
      text: textValue,
      editedAt: Date.now()
    }
    dispatch(editMessage(newMessage));
    onClose();
  }

  useEffect(() => {
    if (message) setTextValue(message.text);
  }, [message]);

  return (
    <div
      onClick={onClickClose}
      ref={modalRef}
      className={`edit-message-modal${isOpen ? " modal-shown" : ""}`}>
      <div className="edit-message-modal-body">
        <h2 className="edit-message-title">Edit message</h2>
        <form className="edit-message-form" onSubmit={onEditMessage}>
          <textarea
            className="edit-message-input"
            value={textValue}
            onChange={e => setTextValue(e.target.value)} />
          <div className="edit-message-buttons">
            <button
              className="edit-message-button"
              type="submit">
              edit
            </button>
            <button
              className="edit-message-close"
              type="button"
              onClick={onClose}>
              cancel
            </button>
          </div>
        </form>
      </div>
    </div >
  )
}

