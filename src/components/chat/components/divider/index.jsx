import React from "react";
import "./style.scss";

export default function Divider({ date }) {

  function dateText(date) {
    const dateNow = new Date().getDate();
    const inputDate = date.replace(/\D/g, "");

    switch (dateNow - inputDate) {
      case 0: return 'Today';
      case 1: return 'Yesterday';
      default: return date;
    }
  }

  return (
    <div className="messages-divider">
      <div className="messages-divider-date">{dateText(date)}</div>
    </div>
  )
}