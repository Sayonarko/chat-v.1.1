import React, { useState } from "react";
import "./style.scss";

export default function MessageInput({ handleAddMessage }) {
  const [text, setText] = useState("");

  function addMessage(e) {
    e.preventDefault();
    if (text) {
      const message = {
        text,
        user: "superuser",
        createdAt: Date.now(),
        editedAt: "",
        id: Math.floor(Math.random() * 100000)
      }
      handleAddMessage(message);
      setText("");
    }
  }

  return (
    <div className="message-input">
      <form onSubmit={addMessage}>
        <input
          className="message-input-text"
          type="text"
          autoFocus
          placeholder="Type your message..."
          value={text}
          onChange={e => setText(e.target.value)} />
        <button
          className="message-input-button"
          type="submit">
          send
        </button>
      </form>
    </div>
  )
}