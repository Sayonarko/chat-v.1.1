import React from "react";
import { getTime } from "../../../../helpers";
import { useDispatch } from "react-redux";
import { showEditModal } from "../../../../redux/actions/editModal";

import "./style.scss";

export default function OwnMessage({ message, handleDeleteMessage }) {
  const dispatch = useDispatch();
  const { text, editedAt, createdAt, id } = message;
  const formattedTime = editedAt ? getTime(editedAt) : getTime(createdAt);

  const onDeleteMessage = () => handleDeleteMessage(id);
  const onEditMessage = () => dispatch(showEditModal(message));

  return (
    <div className="own-message">
      <div className="message-text">{text}</div>
      <div className="message-body-bottom">
        <div className="message-buttons">
          <button
            className="message-edit"
            onClick={onEditMessage}>
            edit
          </button>
          <button
            className="message-delete"
            onClick={onDeleteMessage}
          >
            delete
          </button>
        </div>
        <div className="message-time">{editedAt ? "edited " : ""}{formattedTime}</div>
      </div>
    </div>
  )
}