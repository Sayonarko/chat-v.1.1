import React from "react";
import logo from "../../img/logo.svg";
import "./style.scss";

export default function Header() {

  return (
    <div className="main-header">
      <a href="/">
        <img src={logo} alt="logo" />
      </a>
    </div>
  )
}