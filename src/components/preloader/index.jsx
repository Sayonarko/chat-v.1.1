import React from "react";
import { CircularProgress } from "@material-ui/core";
import { useSelector } from "react-redux";

import "./style.scss";

export default function Preloader() {
  const isLoading = useSelector(state => state.chat.preloader);

  return (
    <div className="preloader" style={{ display: isLoading ? "flex" : "none" }}>
      <CircularProgress size="100px" />
    </div>
  )
}